const express = require("express");
const { reverse } = require("methods");
const app = express();
const port = 8001;

app.get("/", function(req, res) {
    res.setHeader("X-Hello-world", "morning fahmi");
    res.status(200).send("Morning Fahmi");
});

app.get("/product", function(req, res) {
    res.setHeader("X-Hello-world", "Product release");
    res.status(200).send(["Apple", "Redmi", "One Plus"]);
});

app.get("/order", function(req, res) {
    res.status(200);
    res.json([
        {
        id: 1,
        paid: false,
        user_id: 1,
    },
    {
        id: 2,
        paid: false,
        user_id: 1,
    },
    ]);
});

app.get("/user",  function(req, res) {
    res.status(200);
    res.json({
        name: req.query.name,
        job: req.query.job,
        address: `lived in ${req.query.address}`,
    })
})


app.listen(port, () => console.log(`web app ini http://localhost ${port}`))