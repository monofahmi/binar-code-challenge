//initiate const
const express = require("express");
const morgan = require("morgan");
const app = express();
const port = 8001;
const path = require('path');

let dataUser = require("./data-user.json");


//initiate view engine
app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(express.urlencoded({extended : false}));
app.use(express.json());

//initiate error handler
app.use(function(err, req, res, next) {
    res.status(404);
    res.json({
        status: "Fail to load",
        errors: "Can't Open Yet, Please Consider Your Endpoint",
    });
    return;
});

//initiate page
app.get('/Homepage', (req, res) => {
    res.status(200);
    res.render('Homepage');
});

app.get('/Games', (req, res) => {
    res.status(200);
    res.render('Games');
});

app.get('/user-data', (req, res) => {
    res.status(200);
    res.json(dataUser);
});

app.post('/signup', (req, res) => {
    const { username, password } = req.body;
    if (!req.body.username) {
        res.status(400).json({
            "messages": "Please insert username",
        });
        return;
    };
    if (!req.body.password) {
        res.status(400).json({
            "messages": "Please insert password",
        });
        return;
    };
    const id = dataUser[dataUser.length - 1].id + 1;
    const add = {id, username, password}
    dataUser.push(add);
    res.status(200).json(add);
});

app.post('/login', (req, res) => {
    let user = dataUser.find(function(user){
        return user.username === req.body.username && user.password === req.body.password;
    });
    if (!user){
        res.status(401).json("Forgot your Username/Password?");
        return;
    };
    res.status(200).json(user);
})


//initiate console log
app.listen(port, () => console.log(`You are using Web app in http://localhost:${port}`));