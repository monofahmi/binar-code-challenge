function getComChoose() {
//com logic
const com = Math.random();
if (com < 0.34)
    return 'rock';
if (com >= 0.34 && com < 0.67) 
    return 'paper';
    return 'scissors';
}

function getResult(player, com) {
//match logic
if ( player == com) return 'DRAW';
if ( player == 'rock') return ( com == 'scissors') ? 'WIN': 'LOSE';
if ( player == 'paper') return ( com == 'rock') ? 'WIN': 'LOSE';
if ( player == 'scissors') return ( com == 'paper') ? 'WIN' : 'LOSE';

}

//selection in button player

//rock
const playerRock = document.getElementById('rock');
playerRock.addEventListener('click', function() {
    let comChoose = getComChoose ();
    resetComSelected();
    if (comChoose == 'rock') {
        comRock.classList.add("selected")
    };
    if (comChoose == 'paper') {
        comPaper.classList.add("selected")
    };
    if (comChoose == 'scissors') {
        comScissors.classList.add("selected")
    };
    const playerChoose = playerRock.id;
    const result = getResult(playerChoose, comChoose);
    if (result == 'WIN') {
        textResult.innerText = "PLAYER 1 WIN"
    };
    if (result == 'LOSE') {
        textResult.innerText = "COM WIN"
    };
    if (result == 'DRAW') {
        textResult.innerText = "DRAW"
    };
    textResult.classList.add("match-condition");
    
    resetSelected();
    playerRock.classList.add("selected");

    //console.log('player :' + playerChoose);
    //console.log('com :' + comChoose);
    //console.log('hasil :' + result);
    //untuk melihat cetakan di console
})

//paper
const playerPaper = document.getElementById('paper');
playerPaper.addEventListener('click', function() {
    let comChoose = getComChoose ();
    resetComSelected();
    if (comChoose == 'rock') {
        comRock.classList.add("selected")
    };
    if (comChoose == 'paper') {
        comPaper.classList.add("selected")
    };
    if (comChoose == 'scissors') {
        comScissors.classList.add("selected")
    };
    const playerChoose = playerPaper.id;
    const result = getResult(playerChoose, comChoose);
    if (result == 'WIN') {
        textResult.innerText = "PLAYER 1 WIN"
    };
    if (result == 'LOSE') {
        textResult.innerText = "COM WIN"
    };
    if (result == 'DRAW') {
        textResult.innerText = "DRAW"
    };
    textResult.classList.add("match-condition");
    resetSelected();
    playerPaper.classList.add("selected");
})
//scissors
const playerScissors = document.getElementById('scissors');
playerScissors.addEventListener('click', function() {
    let comChoose = getComChoose ();
    resetComSelected();
    if (comChoose == 'rock') {
        comRock.classList.add("selected")
    };
    if (comChoose == 'paper') {
        comPaper.classList.add("selected")
    };
    if (comChoose == 'scissors') {
        comScissors.classList.add("selected")
    };
    const playerChoose = playerScissors.id;
    const result = getResult(playerChoose, comChoose);
    if (result == 'WIN') {
        textResult.innerText = "PLAYER 1 WIN"
    };
    if (result == 'LOSE') {
        textResult.innerText = "COM WIN"
    };
    if (result == 'DRAW') {
        textResult.innerText = "DRAW"
    };
    textResult.classList.add("match-condition");
    resetSelected();
    playerScissors.classList.add("selected");
})

//selection button in com

//com
const comRock = document.getElementById('com-rock');
const comPaper = document.getElementById('com-paper');
const comScissors = document.getElementById('com-scissors')


//text match result
const textResult = document.getElementById("text-result");

//function reset selected
function resetSelected() {
    playerRock.classList.remove("selected");
    playerPaper.classList.remove("selected");
    playerScissors.classList.remove("selected");
}

function resetComSelected() {
    comRock.classList.remove("selected");
    comPaper.classList.remove("selected");
    comScissors.classList.remove("selected");

}

function resetTextResult() {
    textResult.classList.remove("match-condition");
    textResult.innerText = "VS";
    textResult.className = "who-win"
}

//reload
const btnRefresh = document.getElementById('refresh');
btnRefresh.addEventListener('click', function() {
    resetSelected();
    resetComSelected();
    resetTextResult();
})
